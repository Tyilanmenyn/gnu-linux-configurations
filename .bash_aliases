
if [[ ! $EUID -eq 0 ]]; then
  
  # Add sudo to the following commands, only if we're not root
  alias apachectl="sudo apachectl"
  alias apt-get="sudo apt-get"
  alias pacman="sudo pacman"
  alias wifi-menu="sudo wifi-menu"

  # ACPI functions
  alias pm-hibernate="sudo pm-hibernate"
  alias pm-suspend="sudo pm-suspend"

fi

# Aliases which should always be enabled
alias mkdir="mkdir -p"
#alias ls="ls --color=auto"
alias gtk++="g++ `pkg-config --cflags --libs gtkmm-3.0`"
alias vbox="VBoxManage"

